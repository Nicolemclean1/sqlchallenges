use challenge_nm;
DROP PROCEDURE IF EXISTS tickerchallengep;
DELIMITER //
CREATE PROCEDURE tickerchallengep (in ticker text, in indate varchar(20))
Begin
-- set @indate='2010-10-11 09:00';
set @indate=indate;
SELECT `<ticker>`,
SUM(`<vol>`*`<close>`)/SUM(`<vol>`) as VOLUME_WEIGHTED_PRICE,
DATE_FORMAT(@indate, '%d/%m/%Y') as date,
DATE_FORMAT(@indate,'%H%i') as start,
date_format(date_add(@indate, interval 5 hour),'%H%i') as endtime
from tickerinfo
where str_to_date(`<date>`,'%Y%m%d%H%i') between str_to_date(@indate,'%Y-%m-%d %H:%i') and date_add(str_to_date(@indate,'%Y-%m-%d %H:%i'), interval 5 hour);
end //
delimiter ;
