use challenge2_nm;
DROP PROCEDURE IF EXISTS c2;
DELIMITER //
CREATE PROCEDURE c2()
Begin

create temporary table temporarytable1 (
    `date` text,
    maxrange varchar(50)
    );
    
insert into temporarytable1 (maxrange,`date`)
select distinct(Abs(open - close)) as maxrange, `date`
from ibmpriceinfo ORDER BY maxrange desc limit 3;


 create temporary table temporarytable2 (
	 maxprice varchar(50),
     `date` text
    );
    
     insert into temporarytable2 (maxprice, `date`)
 select max(high),date
 from ibmpriceinfo 
 where date in (select `date` from temporarytable1)
 group by date;
 
 
create temporary table temporarytable3
	(`date` text,
    MaxTime text
    );
insert into temporarytable3 (
select a.date, date_format(str_to_date(time, "%H%i"),"%H:%i") as MaxTime
from ibmpriceinfo as a
inner join temporarytable2 as b on 
a.date = b.`date` 
and a.high = b.maxprice );


 create temporary table finishedtable
	(`date` text,
    maxrange varchar(50),
    MaxTime text
    );
    
  
     select a.`date`, a.maxrange, b.MaxTime
 from temporarytable1 as a 
 inner join temporarytable3 as b on
 a.`date` = b.`date`;

drop table temporarytable1;
drop table temporarytable2;
drop table temporarytable3;
drop table finishedtable;

 
end //
delimiter ;
